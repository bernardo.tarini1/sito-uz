---
layout: page
title: Olimpiadi della Matematica
permalink: /olimpiadi/
---

[Sito ufficiale](https://olimpiadi.dm.unibo.it/) e [forum ufficiale](https://www.oliforum.it/) delle ITAMO 
(Olimpiadi Italiane di Matematica)

[Videolezioni](https://olimpiadi.dm.unibo.it/videolezioni/) stage di matematica olimpica pisani (Senior, WC, PreIMO).

[Sito ufficiale](https://www.imo-official.org/) delle IMO (International Mathematical Olympiads)

Forum internazionale Olimpiadi della Matematica: [AoPS](https://artofproblemsolving.com/community/c13_contests) (Art of Problem Solving)