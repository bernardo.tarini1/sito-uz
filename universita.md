---
layout: page
title: Università
permalink: /universita/
---


### Tesi triennale

La mia tesi triennale si concentra sul teorema di Kac (1980), riguardante la teoria delle rappresentazioni di quiver, e propone un collegamento con le algebre di Kac-Moody. 

Il lavoro svolto è pubblicamente accessibile:
- [repository](https://gitlab.com/bernardotarini/tesi-triennale)
- [tesi](https://bernardotarini.gitlab.io/tesi-triennale/tt.pdf)

### Tesi magistrale

La mia tesi magistrale si concentra sul calcolo di anelli di Chow a coefficienti interi di spazi classificanti di alcuni gruppi algebrici classici. 

Il lavoro svolto è pubblicamente accessibile:
- [repository](https://gitlab.com/bernardotarini/tesi-magistrale)
- [tesi](https://bernardotarini.gitlab.io/tesi-magistrale/tm.pdf)

