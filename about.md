---
layout: page
title: About
permalink: /about/
---

Questo sito è stato realizzato con [Jekyll](https://jekyllrb.com/) e utilizza il tema [minima](https://github.com/jekyll/minima).
Il codice sorgente è pubblicamente disponibile [qui](https://gitlab.com/bernardo.tarini1/sito-uz).
