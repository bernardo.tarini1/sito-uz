---
layout: page
title: Altro
permalink: /altro/
---

### Collegamenti Utili

- [Homepage di Eduardo Venturini](https://ventu06.gitlab.io/)
- [Appunti (vari corsi) di Marco Vergamini](https://poisson.phc.dm.unipi.it/~vergamini/)
