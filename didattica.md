---
layout: page
title: Didattica
permalink: /didattica/
---

### Reading Group: Intersection Theory on Moduli Spaces

- [04-03-2025 (Domenico Marino)](https://bernardotarini.gitlab.io/IT_Moduli_Spaces/04-03-25-Marino-Grothendieck_Topologies.pdf): Grothendieck topologies
- [11-03-2025 (Francesco Sorce)](https://bernardotarini.gitlab.io/IT_Moduli_Spaces/11-03-25-Sorce-Fibered_Categories_Stacks.pdf): Fibered categories and stacks over sites
- [18-03-2025 (Pietro Leonardini)](): 

