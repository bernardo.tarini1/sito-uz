---
layout: page
title: Home
permalink: /
---

Sono un dottorando di Matematica iscritto al corso di perfezionamento presso la [Scuola Normale Superiore](https://www.sns.it/)
sotto la supervisione del Professor [Angelo Vistoli](https://homepage.sns.it/vistoli/). 
Ho conseguito il ciclo ordinario presso la Scuola Normale Superiore e presso l'[Università di Pisa](https://www.dm.unipi.it/). 



### Curriculum Vitae

Scaricabile [qui](https://bernardotarini.gitlab.io/curriculum-vitae/cv.pdf).


### Contatti

{% for contact in site.data.data.contacts %}
<i class="{{ contact.icon }}"></i> [{{ contact.name }}]({{ contact.link }})
{% endfor %}
